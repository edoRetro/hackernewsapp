# HackerNewsApp

**Description**

This project is part of a task, for a selection process of a company called Reign. It's a application to show nodeJS articles from a server, each one can be deleted, and clicked to open a new browser tab with a link to the article.

BACK-END: NestJS + NodeJS + MongoBD

FRONT-END: Angular

**INSTALLATION**

- Download this repo to a local folder.
- You should have Docker and Docker Compose installed on your computer.
- Open a new terminal in the project's root.
- Type _docker-compose build --no-cache_ and press return.
- When finished, type _docker-compose up_
- When finished, the app should be running on localhost:4200.
- For the first time use, we need to populate the DB. The server gets data and saves it in mongoDB each hour, but the first time we need to force this behaviour. So, if you're using macOS, open Docker Desktop and select the volume called "nodejs-server", and open a cli terminal. You can do this also by shell commands.
- Inside this container shell, type "npm run execute populateDB", and press return. Wait until you get a confirmation message (NOW DATABASE HAS DATA!), and now you are good to go!
- Open a web browser tab and go to localhost:4200. The app should load and show the listed articles <3
- Have fun!
