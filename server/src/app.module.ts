import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ScheduleModule } from "@nestjs/schedule";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ArticleModule } from "./article/article.module";

const url = process.env.MONGO_URL || "localhost";

@Module({
  imports: [
    ArticleModule,
    MongooseModule.forRoot(`mongodb://${url}:27017/HNArticles`),
    ScheduleModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
