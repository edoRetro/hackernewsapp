import { Controller, Get, HttpStatus, Res } from "@nestjs/common";
import { ArticleService } from "./article.service";

@Controller("articles")
export class ArticleController {
  constructor(private articleService: ArticleService) {}

  @Get("/")
  async getArticles(@Res() res) {
    const data = await this.articleService.getArticles();
    res.status(HttpStatus.OK).json({
      data,
    });
  }
}
