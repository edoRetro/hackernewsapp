import { Injectable } from "@nestjs/common";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Article } from "./interface/article.interface";
import { ArticleDTO } from "./dto/article.dto";
import { Cron, CronExpression } from "@nestjs/schedule";
import { HttpService } from "@nestjs/axios";
import { firstValueFrom } from "rxjs";

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel("Article") private articleModel: Model<Article>,
    private httpService: HttpService
  ) {}

  async getArticles(): Promise<Article[]> {
    const articles = await this.articleModel.find();
    return articles;
  }

  async addArticle(newArticle: ArticleDTO): Promise<Article> {
    const articleSaved = new this.articleModel(newArticle);
    return await articleSaved.save();
  }

  async addArticles(newArticles: ArticleDTO[]): Promise<string> {
    await this.clearArticles();
    await this.articleModel.insertMany(newArticles);
    return "OK";
  }

  @Cron(CronExpression.EVERY_HOUR)
  async populateDatabase() {
    const response = await firstValueFrom(
      this.httpService.get(
        "https://hn.algolia.com/api/v1/search_by_date?query=nodejs"
      )
    );
    let outputData: ArticleDTO[] = response.data.hits;
    let saveResponse = await this.addArticles(outputData);
    console.log("Db save state: " + saveResponse);
  }

  async clearArticles() {
    return await this.articleModel.deleteMany({});
  }
}
