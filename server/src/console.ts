import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ArticleService } from "./article/article.service";

async function bootstrap() {
  const application = await NestFactory.createApplicationContext(AppModule);

  const command = process.argv[2];

  switch (command) {
    case "populateDB":
      const articleService = application.get(ArticleService);
      await articleService.populateDatabase();
      console.log("Now database has data!");

      break;
    default:
      console.log("Command not found");
      process.exit(1);
  }

  await application.close();
  process.exit(0);
}

bootstrap();
