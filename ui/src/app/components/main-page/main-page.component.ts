import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/article.interface';
import { ArticleService } from 'src/app/services/articles/article.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ height: 0, opacity: 0 }),
            animate('.3s ease-out',
                    style({ height: 300, opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ height: 300, opacity: 1 }),
            animate('.2s ease-in',
                    style({ height: 0, opacity: 0 }))
          ]
        )
      ]
    )]
})
export class MainPageComponent implements OnInit {
  articleList: Article[] = [];

  constructor(private articleService: ArticleService) {}

  ngOnInit(): void {
    this.articleService.getDeletedItems();
    this.articleService.getAll().subscribe((response) => {
      console.log(response);

      this.articleList = response;
      this.articleList = this.articleList.filter(
        (el) =>
          this.articleService.deletedItems.find(
            (deleted) => deleted === el._id
          ) == null
      );
    });
  }

  deleteElement(article: Article): void {
    this.articleService.delete(article._id);
    this.articleList = this.articleList.filter((el) => el._id !== article._id);
  }

  clickElement(article: Article): void {
    window.open(article.story_url ? article.story_url : article.url, '_blank');
  }
}
