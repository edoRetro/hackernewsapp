import { Article } from './article.interface';

export interface GetArticleResponse {
  data: Article[];
}
