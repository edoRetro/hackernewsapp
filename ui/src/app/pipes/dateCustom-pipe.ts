import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateCustom',
})
export class DateCustomPipe implements PipeTransform {

  monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December'];

  transform(value: Date): string {
    const parsedDate = new Date(value);
    const differenceDays = new Date().getDate() - parsedDate.getDate();

    return differenceDays === 0 ? this.str_pad(parsedDate.getHours()) + ':' + this.str_pad(parsedDate.getMinutes()) :
           differenceDays === 1 && differenceDays < 2 ? 'Yesterday' :
           differenceDays >= 2 ? this.monthNames[parsedDate.getMonth()] + ' ' + parsedDate.getDate() : '';
  }

  str_pad(n): string {
    return String('00' + n).slice(-2);
  }
}
