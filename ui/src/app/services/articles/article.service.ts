import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Article } from 'src/app/interfaces/article.interface';
import { GetArticleResponse } from 'src/app/interfaces/getArticleResponse';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  constructor(private httpClient: HttpClient) {}

  deletedItems: string[] = [];

  getAll(): Observable<Article[]> {
    const endpoint = 'http://localhost:3000/articles/';
    const headers = {
      'Content-Type': 'application/json',
    };
    const options = {
      headers,
    };
    return this.httpClient.get<GetArticleResponse>(endpoint, options).pipe(
      map((res) => res.data),
      map((data) =>
        data.sort(
          (a, b) =>
            new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        )
      ),
      map((data) => {
        return data.filter((el) => el.story_title != null || el.title != null);
      })
    );
  }

  getDeletedItems(): void {
    this.deletedItems = JSON.parse(localStorage.getItem('deleted'));
    if (this.deletedItems == null) {
      this.deletedItems = [];
    }
  }

  delete(elementID: string): void {
    this.deletedItems.push(elementID);
    localStorage.setItem('deleted', JSON.stringify(this.deletedItems));
  }
}
